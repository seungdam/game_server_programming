#pragma once
#define WIN32_LEAN_AND_MEAN

// 여기에 미리 컴파일하려는 헤더 추가
#ifdef _DEBUG
#pragma comment(lib,"Debug\\server_core.lib")
#else
#pragma comment(lib,"Release\\server_core.lib")
#endif

#include "CorePch.h"