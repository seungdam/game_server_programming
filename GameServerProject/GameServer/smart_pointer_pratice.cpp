
#include "pch.h"
#include "CorePch.h"


class Knight;
class Inventory;

using SharedKnight = SharedPtr<Knight>;
using SharedInventory = SharedPtr<Inventory>;

class Inventory : public RefCountable
{
public:
	Inventory(SharedKnight k) : _k(**k) { cout << "Inventory()" << endl; }
	~Inventory() { cout << "~Inventort()" << endl; }
public:

	// 정상 케이스
	Knight& _k;
	// 비정상 케이스
	//SharedKnight _k;
};
// 스마트 포인터 순환 이슈 : 서로 래퍼런스 카운트를 물고 물어 영원히 해제되지 않는 상태
class Knight : public RefCountable
{
public:
	static int id;
public:
	Knight() 
	{ 
		_id = id++;
		cout << _id << " Kngiht()" << endl; 
	}
	~Knight() 
	{ 
		cout << _id << " ~Kngiht()" << endl;
		//_inv->ReleaseRef();
	}

	void SetTarget(SharedKnight other)
	{
		// other ref 2
		_target = other;
		// other ref 3
		// 소멸 other ref 2
	}
public:
	SharedInventory _inv = nullptr;
private:
	int32 _id = 0;
	int32 _hp = 150;
	SharedKnight _target = nullptr;
	
};

int Knight::id = 0;

int main() 
{
	
	SharedKnight k1(new Knight());
	SharedKnight k2(new Knight());
	k1->ReleaseRef();
	k2->ReleaseRef();

#pragma region 정상 케이스
	// k1 ref: 1  k2 ref: 1
	//k1->SetTarget(k2);
	// k1 ref: 1 k2 ref: 2

	//k1 = nullptr;  // 0 k1 에서 소멸하면서 자기 자신의 레퍼런스 카운트랑 타겟으로 가지고 있던 k2 레퍼런스 카운트 1 감소.
	//k2 = nullptr; //  k2 소멸하면서 레퍼런스 카운트 1 감소 0
#pragma endregion

#pragma region 순환 케이스
	k1->SetTarget(k2); // k1 ref 1 k2 ref 2
	k2->SetTarget(k1); // k1 ref 2 k2 ref 2
	k1 = nullptr; // k1 ref 1 k2 ref 2
	k2 = nullptr; // k1 ref 1 k2 ref 1
	// 아무도 사용을안하는데 소멸자가 호출되지 않는다.
#pragma endregion

	SharedKnight k3(new Knight());
	k3->ReleaseRef(); 
	// k3 -> ref 1

	// inv -> ref 1 k3 -> ref 2
	k3->_inv = new Inventory(k3);  // k3-> ref 1 inv-> ref1
	k3 = nullptr;
}

