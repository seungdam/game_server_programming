#include "pch.h"
#include "ThreadManager.h"
#include "Memory.h"

using namespace std;
class Player
{
public:
	Player() {};
	~Player() {};
};
class Knight : public Player
{
public:
	Knight() { cout << "생성자"; }
	~Knight() { cout << "소멸자"; }
public:
	int32 _hp;
	int32 _skil;
};

int main()
{

	// 다양한 stl 컨테이너를 사용해서 작업을 할 때, 우리가 내부적으로 뭔가 조치를 하지 않는 이상, 지들 임의로 
	// 동적할당을 진행할 것이다.
	// 이런 컨테이너들을 f12를 통해 정의를 확인해보면 Allocator를 인자로 받는 것을 알 수 있다!?
	// ==> 우리만의 임의의 Allocator를 정의해줄 수 있다는 뜻이다.
	// ==> 하지만 아무 Allocator만 되는게 아니다. 형식이 지정되어 있다.
	/*vector<Knight, STLAllocator<Knight>> v(10);
	Map<int32, Knight> m;
	m[100] = Knight();*/

	for (int32 a = 0; a < 1; ++a)
	{
		GThreadManager->Launch([]() 
			{
				while (true)
				{
					Vector<Knight> v(10);
				
				}
				this_thread::sleep_for(1s);
			});
	}
	GThreadManager->Join();
}