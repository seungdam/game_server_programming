#pragma once
//[32][64][][][][]

struct MemoryHeader
{
	MemoryHeader(int32 size): allocSize(size) {};

	static void* AttachHeader(MemoryHeader* header, int32 size)
	{
		new(header)MemoryHeader(size);
		return reinterpret_cast<void*>(++header); // 헤더는 우리만 내부적으로 관리하고 실제 반환시켜주는건 
		// 데이터 시작 지점 뿐
	}

	static MemoryHeader* DetachHeader(void* ptr)
	{
		MemoryHeader* header = reinterpret_cast<MemoryHeader*>(ptr) - 1;
		return header;
	}
	//[MemoryHeader][Memory]
	int32 allocSize; // 데이터 할당 사이즈

};
class MemoryPool
{
public:
	MemoryPool(int32 allocSize);
	~MemoryPool();
	void Push(MemoryHeader* ptr); // 메모리 다 사용 후 재사용하기 위해 저장소에 보관
	MemoryHeader* Pop();
private:
	int32 _allocSize = 0; // 자신이 담당하는 메모리 사이즈
	Atomic<int32> _allocCount = 0; // 자신이 할당한 메모리 갯수가 몇 개인지?
	USE_LOCK;
	queue<MemoryHeader*> _q;
};

