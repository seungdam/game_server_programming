#include "pch.h"
#include "CoreGlobal.h"
#include "ThreadManager.h"
#include "Memory.h"

ThreadManager* GThreadManager = nullptr;
MemoryManager* GMemory = nullptr;

class CoreGlobal
{
public:
	CoreGlobal()
	{
		GThreadManager = new ThreadManager();
		GMemory = new MemoryManager();

	}

	~CoreGlobal()
	{
		delete GThreadManager;
		delete GMemory;
	}
} GCoreGlobal;