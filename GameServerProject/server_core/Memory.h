#pragma once
#include "Allocator.h"

class MemoryPool;


// 메모리 관리 코드
class MemoryManager
{
	enum
	{
		// ~1024까진 32 단위 ~2048까지 128단위 ~4096까지 256단위로 생성해서 풀링
		POOLCOUNT = (1024 / 32) + (2048 /128) + (4096 / 256),
		MAX_ALLOC_SIZE = 4096
	};
public:
	MemoryManager();
	~MemoryManager();

	void* Allocate(int32 size);
	void Release(void* ptr);
private:
	vector<MemoryPool*> _pools;

	// 메모리 크기 <-> 메모리 풀
	MemoryPool* _poolTable[MAX_ALLOC_SIZE + 1];
};

// 특정 클래스에 대해서 메모리 할당 + 생성자 인자를 넘겨주도록하자 // 가변 인자를 사용
template <class T , class... Args>
T* xnew(Args&&... args) // 보편 참조 나중에 따로 공부해보자
{
	T* memory = static_cast<T*>(Xalloc(sizeof(T))); // 메모리 영역만 할당해주기 때문에 생성자를 직접 호출해야한다.
	// placement new // 이미 있는 메모리에다가 생성자 호출까지 같이 해보자.
	new(memory)T(forward<Args>(args)...);
	return memory;
}


template<class T>
void xdelete(T* ptr)
{
	ptr->~T(); // 삭제 전에 소멸자를 호출해 준다.
	Xrelease(ptr);
}
