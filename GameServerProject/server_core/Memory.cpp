#include "pch.h"
#include "Memory.h"
#include "MemoryPool.h"

MemoryManager::MemoryManager()
{
	int32 size = 0;
	int32 tableIdx = 0;
	for (size = 32; size < 1024; size += 32)
	{
		MemoryPool* pool = new MemoryPool(size);
		_pools.push_back(pool);
		while (tableIdx <= size)
		{
			_poolTable[tableIdx] = pool;
			tableIdx++;
		}
	}

	for (; size < 2048; size += 128)
	{
		MemoryPool* pool = new MemoryPool(size);
		_pools.push_back(pool);
		while (tableIdx <= size)
		{
			_poolTable[tableIdx] = pool;
			tableIdx++;
		}
	}

	for (; size < 4096; size += 256)
	{
		MemoryPool* pool = new MemoryPool(size);
		_pools.push_back(pool);
		while (tableIdx <= size)
		{
			_poolTable[tableIdx] = pool;
			tableIdx++;
		}
	}
}

MemoryManager::~MemoryManager()
{
	for (MemoryPool* pool : _pools)
		delete pool;
	_pools.clear();
}

void* MemoryManager::Allocate(int32 size)
{
	MemoryHeader* header = nullptr;
	const int32 allocSize = size + sizeof(MemoryHeader);
	if (allocSize > MAX_ALLOC_SIZE)
	{
		// 메모리 풀링 최대 사이즈를 벗어날 경우 그냥 일반 동적할당 수행
		header = reinterpret_cast<MemoryHeader*>(::malloc(allocSize));
	}
	else
	{
		header = _poolTable[allocSize]->Pop();
	}
	return MemoryHeader::AttachHeader(header, allocSize);
}

void MemoryManager::Release(void* ptr)
{
	MemoryHeader* header = MemoryHeader::DetachHeader(ptr);

	const int32 allocSize = header->allocSize;
	ASSERT_CRASH(allocSize > 0);
	if (allocSize > MAX_ALLOC_SIZE)
	{
		::free(header); // ㅈㄴ 큰 메모리면 일반 해제
	}
	else
	{
		_poolTable[allocSize]->Push(header);
	}
}
