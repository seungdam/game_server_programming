#pragma once

#include "Types.h"
#include "Allocator.h"

#include <vector>
#include <list>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

using namespace std;

template<class T>
using Vector = vector<T, STLAllocator<T>>;

template<class T>
using List = list<T, STLAllocator<T>>;

template <class Key, class T, class Predicate = less<Key>>
using Map = map<Key, T, Predicate, STLAllocator<pair<const Key, T>>>;

template <class Key, class T, class Predicate = less<Key>>
using Set = set<Key, Predicate, STLAllocator<pair<const Key, T>>>;

using string = basic_string<char, char_traits<char>, STLAllocator<char>>;
using wstring = basic_string<wchar_t, char_traits<wchar_t>, STLAllocator<wchar_t>>;

template<class Key, class T, class Hasher = hash<Key>,class KeyEq = equal_to<Key>>
using HashMap = unordered_map<Key, T, Hasher, KeyEq, STLAllocator<pair<const Key, T>>>;