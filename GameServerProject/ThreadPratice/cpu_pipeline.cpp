// 가시성/ 코드 재배치 문제

// 눈에 보이는 것과 실제 코드가 동작하는 과정이 다르기 때문에 결과가 이상하게 나올 수 있음.
// 컴파일러가 일의 효율성을 생각해서 코드를 제멋대로 실행 순서를 제어할 수 있음.
#include <iostream>
#include <thread>
#include "../server_core/Types.h"

using namespace std;

volatile bool ready = false;

int32 a = 0;
int32 b = 0;
int32 r1 = 0;
int32 r2 = 0;

void th1() 
{
	while (!ready) ;

	b = 1;
	r1 = a;
}

void th2() 
{

	while (!ready);
	a = 1;
	r2 = b;
}

int main() 
{
	int32 cnt = 0;
	
	while (true) {
		
		ready = false;
		a = b = r1 = r2 = 0;
		++cnt;
		thread t1(th1);
		thread t2(th2);
		ready = true;
		t1.join();
		t2.join();

		if (r1 == 0 && r2 == 0) break;
	}
	cout << cnt << endl;	
}