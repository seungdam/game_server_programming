#include <windows.h>
#include <iostream>
#include <chrono>
#include "../server_core/Types.h"

using namespace std;
int32 a[10000][10000];

int main() 
{
	//spatial locality ���� test

	int32 sum = 0;
	::ZeroMemory(&a, sizeof(a));
	{
		auto s = chrono::high_resolution_clock::now();
		for (int j = 0; j < 10000; ++j)
			for (int i = 0; i < 10000; ++i)
				sum += a[j][i];
		auto e = chrono::high_resolution_clock::now();

		cout << chrono::duration_cast<chrono::milliseconds>(e - s).count() << endl; 
	}

	{
		auto s = chrono::high_resolution_clock::now();
		for (int j = 0; j < 10000; ++j)
			for (int i = 0; i < 10000; ++i)
				sum += a[i][j];
		auto e = chrono::high_resolution_clock::now();
		cout << chrono::duration_cast<chrono::milliseconds>(e - s).count() << endl;

	}
}