#include <iostream>
#include <mutex>
#include <future>
#include <thread>
#include "../server_core/Types.h"

// future는 기본적으로 활용성이 낮으나, 공식 표준에서 지원하니 알고는 넘어가자
using namespace std;

int32 Calculate() 
{
	int32 sum = 0;
	for (int32 i = 0; i < 100'0000; ++i) ++sum;
	return sum;
}

int32 result;

void PromiseWorker(promise<string>&& pfs) {
	pfs.set_value("Secret Message");
}


void TaskWorker(packaged_task<int32(void)>&& pkg) {
	pkg();
}

int main()
{
	// 동기식 실행
	//int32 msum = 0;
	//msum = Calculate();

	// 비동기식 실행

	//thread t1(Calculate); // 스레드에게 떠넘기고 다른 작업 수행... 그렇다면 반환값은 어떻게 저장할까 공유자원으로 하면 복잡해지고..
						  // 또 이런 잠깐 필요한 작업에 대해서 스레드를 만들어서 작업하는게 효율적일까..? 
	//TO DO
	//t1.join();

	// launch::deferred --> 좀 미뤘다가 수행한다.
	// launch::async --> 백그라운드에서 비동기로 수행한다.
	// 당장은 결과물은 없지만 언젠가 미래에 결과물이 있을 것이라는 일종의 약속을 수행하는 방법
	// 단발성으로 일어나는 이벤트에 관련해서 동기화할 때 활용하면 굉장히 유용하다.
	future<int32> f = async(launch::async, Calculate);

	/*future_status s= f.wait_for(1ms);
	if (s == future_status::ready)
	{
		f.get();
	}*/
	int32 sum = f.get();
	cout << sum << endl;

	// 클래스 멤버함수에서 추출
	class Knight
	{
	public:
		int32 GetHP() { return 100; }
	};
	Knight knight;
	future<int32> kf = async(launch::async, &Knight::GetHP, &knight); // 하나의 간접적인 스레드를 만들어 해치운다.

	sum = kf.get();
	cout << sum << endl;

	//promise
	{
		promise<string> p;
		future<string> sf = p.get_future();
		thread t1(PromiseWorker,move(p));
		t1.join();
		string s = sf.get();
		cout << s << endl;
	}

	//package task

	{
		packaged_task<int32(void)> pt(Calculate); // 일감을 여러 개만들어서 넘겨주는 것도 가능하다.
		future<int32> ft = pt.get_future();
		thread t1(TaskWorker, move(pt));
		
		int32 res = ft.get();
		cout << res << endl;
		t1.join();
	}
}