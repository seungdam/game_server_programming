#include <iostream>
#include <thread>
#include <vector>

using namespace std;


void MyThread() {
	std::cout << "Its My Thread" << std::endl;
}

void MyThread_2(int i) {
	std::cout << i << std::endl; // cout에서는 순서보장이 되지 않기 때문에 출력이 이상적으로 이루어지진 않는다.
}

int main() {

	vector<thread> v;
	for(int i = 0; i < 10; ++i)
		v.push_back(std::thread(MyThread_2, i)); // 빈 객체 먼저 만들고 후에 넣기

	for (auto& i : v) {
		if(i.joinable()) i.join(); 
	}
	
}