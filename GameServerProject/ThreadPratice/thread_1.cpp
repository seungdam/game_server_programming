#include <iostream>
#include <thread>


void MyThread() {
	std::cout << "Its My Thread" << std::endl;
}

void MyThread_2(int i) {
	std::cout << "Its My Thread " << i << std::endl;
}

int main() {
	
	std::thread t; // 해당 스레드가 수행하는 중에 메인 스레드가 종료되면 에러가 생긴다.
	auto id1 = t.get_id(); // 스레드 마다 id 출력
	t = std::thread(MyThread_2 , 10); // 빈 객체 먼저 만들고 후에 넣기
	
	std::cout << "Hello" << std::endl;
	__int32 count = t.hardware_concurrency(); // cpu 코어 개수를 알려준다.
	auto id2 = t.get_id(); // 스레드 마다 id 출력
	
	//t.detach(); // join은 영영 기다려준다고 하면, detach는 우리가 만든 스레드 객체 t와 실질적으로 구동되는 스레드(주스레드)와 연결고리를 끊어
	            //독립적으로 백그라운드에서 수행되도록 한다. --> 해당 스레드의 내용을 추출할 수 있기 때문에 자주 사용하진 않는다.
	 
	if(t.joinable()) // detach 상태이거나 연동된 스레드 여부를 확인하는 함수.
		t.join(); // 이 스레드가 끝날 때 까지 기다려줘야 한다.
}