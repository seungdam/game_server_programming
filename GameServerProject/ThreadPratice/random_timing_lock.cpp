#include <iostream>
#include <thread>
#include <mutex>
#include <atomic>
// sleep을 사용한 locking --> 랜덤 메타
// 무한으로 대기하는 것이 아니라, 자기 자리로 돌아왔다가 일정 시간 후에,  다시 화장실로 이동하는 것과 같음.
// 스케쥴링에 관해서 생각해보자. --> 어떤 프로그램을 먼저 수행할 것인지... 수행 시간은 어떻게 공평하게 부여할 지..

using namespace std;
class SpinLock
{
	atomic<bool> is_available = true;
public:
	void lock() {
		bool expected = true;
		bool desired = false;
		while (is_available.compare_exchange_strong(expected, desired) == false) 
		{
			expected = true;
			this_thread::sleep_for(0ms);
		}
	}
	void unlock() {
		is_available.store(true);
	}
};

int sum = 0;
SpinLock msl;

void add()
{
	for (int i = 0; i < 100'0000; ++i) {
		lock_guard<SpinLock> slg(msl);
		sum++;
	}
}

void sub() {
	for (int i = 0; i < 100'0000; ++i) {
		lock_guard<SpinLock> slg(msl);
		sum--;
	}
}

int main() {
	thread t1(add);
	thread t2(sub);
	t1.join();
	t2.join();

	cout << sum << endl;

}