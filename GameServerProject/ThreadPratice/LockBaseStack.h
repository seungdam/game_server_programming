#pragma once
#include <mutex>
#include <stack>
using namespace std;

// 패킷을 순서대로 처리할 때, 사용할 수 있다.
template<class T>
class LockBaseStack
{
public:
	void Push(T value)
	{
		lock_guard<mutex> lock_g(_m);
		_s.push(std::move(value));
		_cv.notify_one();
	}

	bool TryPop(T& value)
	{
		lock_guard<mutex> lock_lg(_m);
		if (_s.empty()) return false;

		value = std::move(_s.top());
		_s.pop();
		return true;
		// 될 때 까지 무한대로 수행
	}
	void WaitPop(T& value)
	{
		unique_lock<mutex> lock_u(_m);
		_cv.wait(lock_u, [this] {return (_s.empty() == false); });
		value = std::move(_s.top());

		// conditional variable을 사용해 해당 상황이 올 때 까지 기다리는 pop을 작성
	}

	LockBaseStack() {}
	LockBaseStack(const LockBaseStack&) = delete;
	LockBaseStack& operator=(const LockBaseStack& other) = delete;
private:
	stack<T> _s;
	mutex _m;
	condition_variable _cv;
};


