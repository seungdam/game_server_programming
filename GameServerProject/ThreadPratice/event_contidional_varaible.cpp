#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>
#include <windows.h>
#include <queue>

using namespace std;


HANDLE ev;
queue<UINT32> jobs;
mutex m;
condition_variable cv;
int i = 0;
void add_job() 
{
	while (true) 
	{
		{
			unique_lock<mutex> ul(m);
			jobs.push(i++);
		}
		cv.notify_one();
	}

	// 1) lock
	// 2) 공유자원 작업 수행
	// 3) unlock
	// 4) cv로 통지
}




void do_job() {
	while (true)
	{
		unique_lock<mutex> ul(m);
		cv.wait(ul, []() { return (jobs.empty() == false); });
		jobs.pop();
		cout << jobs.size()  << endl;
	}
	// lock과 cv를 묶는다
	// 조건 만족 확인 cv.wait()
	// 조건 만족 시, 락을 잡는다. 안만족하면 unlock하고 대기상태

}

int main() 
{
	
	thread t1(add_job);
	thread t2(do_job);
	t1.join();
	t2.join();
}