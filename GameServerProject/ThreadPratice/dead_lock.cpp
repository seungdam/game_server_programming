#include <iostream>
#include <map>
#include <mutex>
#include <thread>

using namespace std;
class Account
{

};

class User
{

};

class UserManager;

class AccountManager {
public:
	static  AccountManager* Inst()
	{
		static AccountManager instance;
		return &instance;
	}
	Account* GetAccount(uint32_t id) {
		lock_guard<mutex> lg(_m);
		UserManager::Inst()->ProcessSave();
		return nullptr;
	}
	void ProcessLogin() {
		//lock_guard<mutex> lg(_m);

	}
private:
	mutex _m;
	map<uint32_t, Account*> _accounts;
};


class AccountManager;

class UserManager {
public:
	static  UserManager* Inst()
	{
		static UserManager instance;
		return &instance;
	}
	User* GetUser(uint32_t id) {
		lock_guard<mutex> lg(_m);
		AccountManager::Inst()->ProcessLogin();
		return nullptr;
	}
	void ProcessSave() {
		//lock_guard<mutex> lg(_m);
		
	}
private:
	mutex _m;
	map<uint32_t, User*> _users;
};

void fuc1() {
	for (int i = 0; i < 100; ++i) AccountManager::Inst()->GetAccount(100);
}

void fuc2() {
	for (int i = 0; i < 100; ++i) UserManager::Inst()->GetUser(100);
}


int main() {
	thread t1(fuc1);
	thread t2(fuc2);

	t1.join();
	t2.join();

	std::cout << "job finish" << endl;

}