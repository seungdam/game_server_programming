#pragma once
#include <mutex>
#include <stack>
#include <atomic>

template <class T>
struct Node
{
	Node(const T& value) : _data(value)
	{
	}
private:	
	Node* _next;
	T _data;
};

template <class T>
class LockFreeStack
{
public:
	// 1) 새 노드 생성
	// 2) 새 노드의 넥스트를 최근에 추가한 값을 가르키게 하고(head)
	// 3) 새 노드를 헤드로 한다.
	void Push(const T& value)
	{
		Node<T>* n_node = new Node<T>;
		// cas 연산을 사용
		/*n_node->_next = _head;
		n_node->_data = value;
		_head.store(n_node);*/
		while (_head.compare_exchange_weak(n_node->next, n_node) == false)
		{
		}
	}

	// 1) 현재 헤드 노드 읽기
	// 2) 헤드 노드 다음 노드 읽기
	// 3) 헤드 노드를 다음 노드로 변경
	// 4) 현재 노드의 데이터 추출 후 삭제

	bool TryPop(T& value)
	{
		Node<T>* old_node = _head;
	
		while (old_node && _head.compare_exchange_weak(old_node, old_node->_next) == false)
		{
		}
		if (old_node == nullptr) return false;

		delete old_node;
		return true;

	}
private:
	std::atomic<Node<T>*> _head;

};