#include <iostream>
#include <thread>
#include <atomic>
#include "../server_core/Types.h"

using namespace std;
atomic<bool> flag;

atomic<bool> ready;
int32 value;

void Producer() 
{
	value = 10;
	ready.store(true,memory_order_relaxed);
	// ------------ 절취선 -----------------
}

void Consumer()
{
	// ------------------- 절취선 --------------------
	while (ready.load(memory_order_relaxed) == false);
	cout << value << endl;
}

int main()
{

	
	flag.store(true,memory_order_seq_cst);

	{
		// 이 일련의 과정들을 한번에 처리할 수 있는 방법은 없을까?
		//bool prev = flag.load(memory_order_seq_cst); // 주의 만약 prev에 대입하는 과정에서 데이터의 변형이 일어난다면..?
		//flag.store(true, memory_order_seq_cst);
		bool prev =  flag.exchange(true); // 이전 flag값을 prev에 넣고 자신의 값을 true로 바꾼다
	}

	//CAS 조건부 수정
	{
		bool expected = false;
		bool desired = true;
		flag.compare_exchange_strong(expected, desired); // weak와 사용법이 동일하지만 weak의 경우 가짜 실패가 발생한다. 
		// 가짜 실패란? 조건을 만족하더라도 스레드의 침범 등 하드웨어의 인터럽션으로 묘한 상황일 때 실패처리를 수행하는 것
		// while 루프랑 함께 사용하는 것이 일반적이다.
	}
		// 메모리 정책
		// 1) seqientialy consistent (seq_cst)  가장 엄격하다 = 컴파일러 최적화 여지 적음 = 직관적
		//   가시성 및 코드 재배치 문제를 해결할 수 있음
		//   그냥 보편적으로 이걸 사용한다고 보면된다.
		// 
		// 2) Acquire - release (acquire, release)
		//  딱 중간단계 포지션
		//  짝을 맞춰주어야 한다 release -acquire
		//  release 명령 이후 재배치 되는 것을 금지한다.
		//  acquire로 호출하는 순간 이전의 모든 내용들이 이후에 갱신되어 가시성이 보장된다.
		// 
		// 3) relaxed 자유롭다 = 컴파일러 최적화 여지 많음 = 직관적이지 않음
		// 너무 자유럽다 --> 코드 재멋대로 배치 가능 --> 가시성 해결 불가능
		// 가장 기본 조건인 동일 객체의 동일 순서만 보장하는 것

		// intel amd는 애초에 순차적 일관성을 보장하기 때문에 default 값인 seq_cst로 남겨도 별 차이가 없다
		// 하지만 amd의 경우 꽤나 차이가 존재한다.

		// relaxed 와 같은 메모리 베리어 효과를 주는 기능은 atomic 뿐만 아니라 atomic_thread_fence와 같은 것을 사용해 표현할 수 있다만 굳이
		// 사용할 필요는 없다고 볼 수 있음
	ready = false;
	value = 0;
	thread t1(Producer);
	thread t2(Consumer);
	t1.join();
	t2.join();

	
}