#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

using namespace std;

vector<int> v;
// atomic<vector<int>> v;
// RAII 패턴
// --> 어떤 wrapper 클래스를 만들어 생성자에서 잠가주고 소멸자에서 언락해주는 것
mutex v_lock;

template <typename T>
class LockGuard 
{
public:
	LockGuard(T& _l) {
		m_l = &_l;
		m_l->lock();
	};
	~LockGuard() {
		m_l->unlock();
	};
private:
	T* m_l;
};
void push() {
	for (int i = 0; i < 10000; ++i) {
		lock_guard<mutex> lg(v_lock);
		v.push_back(i);
		
	}
}

class SpinLock
{
	atomic<bool> is_available = true;
public:
	void lock() {
		bool expected = true;
		bool desired = false;
		while (is_available.compare_exchange_strong(expected, desired) == false) {
			expected = true;
		}
	}
	void unlock() {
		is_available.store(true);
	}
};

int sum = 0;
SpinLock msl;

void add() 
{
	for (int i = 0; i < 100'0000; ++i) {
		lock_guard<SpinLock> slg(msl);
		sum++;
	}
}

void sub() {
	for (int i = 0; i < 100'0000; ++i) {
		lock_guard<SpinLock> slg(msl);
		sum--;
	}
}

int main() {
	
	thread t1(add);
	thread t2(sub);
	t1.join();
	t2.join();

	cout << sum << endl;
}