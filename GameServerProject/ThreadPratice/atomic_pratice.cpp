#include <iostream>
#include <thread>
#include <atomic>
using namespace std;

atomic<int> cnt = 0;
void add() {
	for (int i = 0; i < 100'0000; ++i) {
		cnt.fetch_add(1);
	}
}

void sub() {
	for (int i = 0; i < 100'0000; ++i) {
		cnt.fetch_sub(1);
	}
}

int main() {
	add();
	sub();
	std::cout << "순차 연산" << cnt << std::endl;
	thread t(add);
	thread t2(sub);

	t.join();
	t2.join();
	std::cout <<"멀티 스레드 연산" << cnt << std::endl; // 엉뚱한 값이 출력
	// 왜?
	// 데이터에 변화를 줄때 한줄짜리라도 어셈블리어로 분석 시 여러줄의 작업을 걸친다.
	// 이때 스레드간의 컨텍스트 스위칭이 발생함에 따라 잘못된 값을 덮어씌우기도 한다는 것.
	// 자원을 여러 스레드에서 가져다 쓸 수 있다는 것은 굉장히 메리트가 있지만, 이를 수정하기 시작하면 문제가 발생한다.
	// 해결법 --> 동기화를 해줘야한다.
	// 여러가지 동기화 기법 중 atomic(원자: 더이상 쪼개지지 않는 최소 단위)이 있다. --> 다 실행되거나 실행이 안되거나 둘 중 하나의 상태만 발생.

	
}