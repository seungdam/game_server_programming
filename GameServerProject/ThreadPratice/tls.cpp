#include <iostream>
#include <thread>
#include <vector>
#include "../server_core/Types.h"

using namespace std;

thread_local int32 LThreadId = 0; // 스레드마다 고유하게 들고 있어야하는 정보들을 담을 때 유용하다. TLS
// job queue에다가도 활용할 수 도 있겠다는 생각이 든다.
// 메모리를 공유하게 하기 위해서 tls에 메모리 큐를 통해 일시적으로 공유 자원을 사용할 수도 있다고 한다.

void threadMain(int32 threadId)
{
	LThreadId = threadId;
	while (1) 
	{
		cout << LThreadId << endl;
		this_thread::sleep_for(1s);
	}
}
int main() 
{
	// tls를 사용해 각 스레드별로 id를 부여하자
	vector<thread> pool;
	for (int i = 0; i < 10; ++i) {
		pool.push_back(thread(threadMain, i));
	}

}