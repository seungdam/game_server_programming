#pragma once
#include <mutex>
#include <queue>


template <class T>
class LockBaseQueue
{
public:
	
	void Push(T value)
	{
		std::lock_guard<std::mutex> lock_lg(_m);
		_q.push(std::move(value));
		_cv.notify_one();
	}

	bool TryPop(T& value)
	{
		std::lock_guard<std::mutex> lock_lg(_m);
		if (_q.empty()) return false;

		value = std::move(_q.front());
		return true;
	}

	void WaitPop(T& value)
	{
		std::unique_lock<std::mutex> lock_u(_m);
		_cv.wait(lock_u, [this] {return (_q.empty() == false); });
		value = std::move(_q.top());
	}
	

	LockBaseQueue() {}
	LockBaseQueue(const LockBaseQueue&) = delete;
	LockBaseQueue& operator=(const LockBaseQueue& other) = delete;
private:
	std::queue<T> _q;
	std::mutex _m;
	std::condition_variable _cv;
};

