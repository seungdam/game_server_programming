#include <iostream>
#include <atomic>
#include <thread>

#include "LockBaseQueue.h"
#include "LockBaseStack.h"
#include "../server_core/Types.h"

using namespace std;

LockBaseQueue<int32> q;
LockBaseStack<int32> s;

void push()
{
	while (1)
	{
		int32 value = rand() % 100;
		q.Push(value);
	}
}
void pop()
{
	while (1)
	{
		int32 data = 0;
		if(q.TryPop(data))
			cout << data << endl;
	}
}

int main() 
{
	thread t1(push);
	thread t2(pop);
	thread t3(pop);

	t1.join();
	t2.join();
	t3.join();
}